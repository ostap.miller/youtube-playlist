module.exports = {
  extends: 'airbnb',
  parser: 'babel-eslint',
  globals: {
    window: true,
    fetch: true,
    document: true,
    navigator: true,
  },
  settings: {
    'import/resolver': {
      node: {
        extensions: ['.js'],
      },
    },
  },
  rules: {
    'react/jsx-filename-extension': ['error', { extensions: ['.js', '.jsx'] }],
    'no-use-before-define': 0, // this is better for writing code from general to details
    'import/no-unresolved': [
      'error',
      {
        ignore: ['~/'],
      },
    ],
    'object-curly-newline': 0, // sometimes object fits into one line
    'arrow-parens': 0, // prettier does either all with parens or all without
    'function-paren-newline': 0, // prettier doesn't have a config for this
    'arrow-body-style': 0, // no problem with this format: ()=>{ return value };
    'global-require': 0,
    indent: 2, // Prettier handles indents
    'no-restricted-syntax': 0,
    'react/require-default-props': 0,
    'linebreak-style': 0, // Git converts linebreak to CRLF for Windows
    'class-methods-use-this': 0,
    'no-underscore-dangle': [2, { allow: ['__'] }],
    'react/no-multi-comp': 0,
    'no-nested-ternary': 0,
    curly: 2,
    'lines-between-class-members': 0,
    'operator-linebreak': 0,
    'react/destructuring-assignment': 0,
    'implicit-arrow-linebreak': 0,
    'no-else-return': 0,
    camelcase: 0,
    'jsx-a11y/click-events-have-key-events': 0,
    'jsx-a11y/interactive-supports-focus': 0,
    'jsx-a11y/no-static-element-interactions': 0,
    'jsx-a11y/label-has-associated-control': 0,
    // Overrides default config, because airbnb don't use type annotations:
    // Enforce component methods order https://github.com/yannickcr/eslint-plugin-react/blob/843d71a432baf0f01f598d7cf1eea75ad6896e4b/docs/rules/sort-comp.md
    'react/sort-comp': [
      'error',
      {
        order: [
          'static-methods',
          'type-annotations',
          'getters',
          'instance-variables',
          'setters',
          'lifecycle',
          '/^on.+$/',
          '/^(get|set)(?!(InitialState$|DefaultProps$|ChildContext$)).+$/',
          'instance-methods',
          'everything-else',
          'rendering',
        ],
        groups: {
          lifecycle: [
            'displayName',
            'propTypes',
            'contextTypes',
            'childContextTypes',
            'mixins',
            'statics',
            'defaultProps',
            'constructor',
            'getDefaultProps',
            'getInitialState',
            'state',
            'getChildContext',
            'componentWillMount',
            'componentDidMount',
            'componentWillReceiveProps',
            'shouldComponentUpdate',
            'componentWillUpdate',
            'componentDidUpdate',
            'componentWillUnmount',
          ],
          rendering: ['/^render.+$/', 'render'],
        },
      },
    ],
  },
};
