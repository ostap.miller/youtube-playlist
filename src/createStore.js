import { createStore as reduxCreateStore, applyMiddleware, compose } from 'redux';
import { createLogger } from 'redux-logger';

import thunk from 'redux-thunk';
import rootReducer from './reducers';

const enhancers = [];
const middleware = [thunk];

if (process.env.NODE_ENV === 'development') {
  if (process.env.BUILD_TARGET === 'web') {
    /* eslint-disable no-underscore-dangle */
    const devToolsExtension =
      window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();
    /* eslint-enable */
    enhancers.push(devToolsExtension);
  }

  const logger = createLogger({
    collapsed: true,
    diff: true,
    titleFormatter: (action, time, took) =>
      `Action: ${action.type}  ${action.key ? `Key: ${action.key} ` : ''}(in ${took.toFixed(
        3,
      )} ms)`,
  });
  middleware.push(logger);
}

const composedEnhancers = compose(
  applyMiddleware(...middleware),
  ...enhancers,
);

const createStore = (initialState = {}) =>
  reduxCreateStore(rootReducer, initialState, composedEnhancers);

export default createStore;
