import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Grid, Image, Button } from 'semantic-ui-react';

import './styles.scss';

export default class Playlist extends PureComponent {
  state = {
    initialScrollDone: false,
  };

  activeVideoRef = React.createRef();
  playlistRef = React.createRef();

  componentDidUpdate() {
    if (!this.state.initialScrollDone && this.props.activeVideoId) {
      this.scrollToSelected(this.props.activeVideoId);
    }
  }

  addDefaultSrc = e => {
    e.target.onerror = null;
    e.target.src = 'https://cdnjs.cloudflare.com/ajax/libs/simple-icons/3.0.1/youtube.svg';
  };

  scrollToSelected = () => {
    if (this.activeVideoRef && this.activeVideoRef.current) {
      this.activeVideoRef.current.scrollIntoView();
      this.setState({ initialScrollDone: true });
    }
  };

  scrollTo = (type = 'right') => {
    if (
      this.playlistRef &&
      this.playlistRef.current &&
      this.activeVideoRef &&
      this.activeVideoRef.current
    ) {
      const newIndex =
        type === 'right' ? this.props.activeVideoIndex + 1 : this.props.activeVideoIndex - 1;

      if (newIndex >= 0 && newIndex <= this.props.videoIds.length - 1) {
        this.playlistRef.current.scrollTo(this.activeVideoRef.current.clientWidth * newIndex, 0);
        this.props.setActiveVideoIndex(newIndex);
      }
    }
  };

  render() {
    const { videoIds, videoData, activeVideoId, activeVideoIndex, path } = this.props;

    return (
      <Grid className="playlistContainer">
        <Button onClick={this.scrollTo} disabled={activeVideoIndex - 1 < 0}>
          {'<'}
        </Button>

        <div ref={this.playlistRef} className="row">
          {videoIds.map(key => (
            <div
              key={key}
              className={`column${key === activeVideoId ? ' active' : ''}`}
              {...key === activeVideoId && { ref: this.activeVideoRef }}
            >
              <Link to={{ path, hash: `${key}` }}>
                <Image
                  alt={videoData[key].title}
                  src={videoData[key].thumbnail}
                  onError={this.addDefaultSrc}
                />
              </Link>
            </div>
          ))}
        </div>

        <Button
          onClick={() => this.scrollTo('right')}
          disabled={activeVideoIndex + 1 > videoIds.length - 1}
        >
          {'>'}
        </Button>
      </Grid>
    );
  }
}

Playlist.propTypes = {
  videoIds: PropTypes.arrayOf(PropTypes.string),
  videoData: PropTypes.instanceOf(PropTypes.object),
  path: PropTypes.string,
  activeVideoId: PropTypes.string,
  activeVideoIndex: PropTypes.number,
  setActiveVideoIndex: PropTypes.func.isRequired,
};
